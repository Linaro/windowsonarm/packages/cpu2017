#------------------------------------------------------------------------------
# SPEC CPU(R) 2017 config file for: Visual Studio on Windows
#------------------------------------------------------------------------------
#
# Usage: (1) Copy this to a new name
#             cd   %SPEC%\config
#             copy Example-x.cfg myname.cfg
#        (2) Change items that are marked 'EDIT' (search for it)
#
# SPEC tested this config file with:
#    Compiler version(s):    Microsoft Visual Studio 2015, Update 3
#                            Microsoft Visual Studio 2017
#                            Microsoft Visual Studio 2019
#    Operating system(s):    Windows 10 Professional (64-bit)
#    Hardware:               Dell Precision Tower 5810, Intel Xeon E5-2650 v3
#                            Intel Core i7-8700K
#
# If your system differs, this config file might not work.
# You might find a better config file at http://www.spec.org/cpu2017/results
#
#  -----------------------------
#  Known Limitations (IMPORTANT)
#  -----------------------------
#            * A 'reportable' run is not possible with Visual Studio
#              unless you add a Fortran compiler.  See the section
#              "No Fortran", below.
#            * Some benchmarks are not expected to work with certain
#              compiler versions.  Search for 'skip' to find details.
#
#   ----    This config file expects 'bits' to be either 32 or 64,
#   bits    according to your compiler "Output Architecture".
#   ----    Examples:
#                -------------------    ------------------------
#                If you use             Then also use
#                -------------------    ------------------------
#                vcvarsall.bat x86      runcpu --define bits=32
#                vcvarsall.bat amd64    runcpu --define bits=64
#                -------------------    ------------------------
#            See Microsoft topic "Setting the Path and Environment Variables
#            for Command-Line Builds" for your version of Visual Studio
#
# Compiler issues: Contact your compiler vendor, not SPEC.
# For SPEC help:   http://www.spec.org/cpu2017/Docs/techsupport.html
#------------------------------------------------------------------------------


#--------- Label --------------------------------------------------------------
# Arbitrary string to tag binaries (no spaces allowed)
#                  Two Suggestions: # (1) EDIT this label as you try new ideas.
%define label "mytest"              # (2)      Use a label meaningful to *you*.


#--------- Preprocessor -------------------------------------------------------
%ifndef %{bits}                # EDIT to match your build type, or set on the
%   define  bits        64     #      runcpu command line.  See topic 'bits'
%endif                         #      above.

%ifndef %{VSVERSION}           # EDIT to match your version, or set on the
%   define  VSVERSION   2019   #      the runcpu command line.  Use a four
%endif                         #      digit number please, such as 2015 or 2017

# Don't change this part.
%if %{bits} == 64
%   define model        x64
%elif %{bits} == 32
%   define model        x86
%else
%   error Please define number of bits - see instructions in config file
%endif
%if %{VSVERSION} < 2015
%   warning This config file was not tested with Visual Studio versions earlier than 2015
%endif
%if %{label} =~ m/ /
%   error Your label "%{label}" contains spaces.  Please try underscores instead.
%endif
%if %{label} !~ m/^[a-zA-Z0-9._-]+$/
%   error Illegal character in label "%{label}".  Please use only alphanumerics, underscore, hyphen, and period.
%endif

#--------- Global Settings ----------------------------------------------------
# For info, see:
#            https://www.spec.org/cpu2017/Docs/config.html#fieldname
#   Example: https://www.spec.org/cpu2017/Docs/config.html#tune

flagsurl             = $[top]/config/flags/MS-VisualStudio.xml
ignore_errors        = 1
iterations           = 1
label                = %{label}-VS%{VSVERSION}-%{model}
mean_anyway          = 1
output_format        = txt,html,cfg,pdf,csv
reportable           = 0
tune                 = base


#--------- How Many CPUs? -----------------------------------------------------
# Both SPECrate and SPECspeed can test multiple chips / cores / hw threads
#    - For SPECrate,  you set the number of copies.
#    - For SPECspeed, you set the number of threads.
# See: https://www.spec.org/cpu2017/Docs/system-requirements.html#MultipleCPUs
#
#    q. How many should I set?
#    a. Unknown, you will have to try it and see!
#
# To get you started, some suggestions:
#
#     copies - This config file defaults to testing only 1 copy.   You might
#              try changing it to match the number of cores on your system,
#              or perhaps the  NumberOfLogicalProcessors as reported by
#                     wmic cpu get /value
#              Be sure you have enough memory.  See:
#              https://www.spec.org/cpu2017/Docs/system-requirements.html#memory
#
#     threads - This config file sets a starting point.  You could try raising
#               it.  A higher thread count is much more likely to be useful for
#               fpspeed than for intspeed.
#
intrate,fprate:
   copies           = 1   # EDIT to change number of copies (see above)
intspeed,fpspeed:
   threads          = 4   # EDIT to change number of OpenMP threads (see above)


#------- Compilers ------------------------------------------------------------
default:
   CC                 = cl /TC
   CLD                = cl
   CXX                = cl /TP
   CXXLD              = cl
   OBJ                = .obj
   # How to say "Show me your version, please"
   CC_VERSION_OPTION   =  "/w"   # There is no specific option for cl to get
   CXX_VERSION_OPTION  =  "/w"   # to get compiler version. Use any valid option.

default:
%if %{bits} == 64
   sw_base_ptrsize = 64-bit
   sw_peak_ptrsize = Not Applicable
%else
   sw_base_ptrsize = 32-bit
   sw_peak_ptrsize = Not Applicable
%endif

#--- No Fortran -------
# Fortran benchmarks are not expected to work with this config file.
# If you wish, you can run the other benchmarks using:
#    runcpu no_fortran           - all CPU 2017 benchmarks that do not use Fortran
#    runcpu intrate_no_fortran   - integer rate benchmarks that do not use Fortran
#    runcpu intspeed_no_fortran  - integer speed benchmarks that do not use Fortran
#    runcpu fprate_no_fortran    - floating point rate benchmarks that do not use Fortran
#    runcpu fpspeed_no_fortran   - floating point speed benchmarks that do not use Fortran
#
# If you *do* have a Fortran compiler, then you will need to set the correct options for
# 'FC' and 'FC_VERSION_OPTION' just above -- see
#                http://www.spec.org/cpu2017/Docs/config.html#makeCompiler
#  You must also remove the 2 lines right after this comment.
any_fortran:
   fail_build = 1


#--------- Portability --------------------------------------------------------
default:               # data model applies to all benchmarks
%if %{bits} == 64
   PORTABILITY = /DSPEC_P64   # data model must be consistent for all of base
%endif

# Benchmark-specific portability (ordered by last 2 digits of bmark number)

500.perlbench_r,600.perlbench_s:
%if %{bits} == 32
   CPORTABILITY      = -DSPEC_WIN32 -DHAS_QUAD
%else
   CPORTABILITY      = -DSPEC_WIN64
%endif

502.gcc_r,602.gcc_s=default:  # When using Visual Studio 2015, skip these.
%if %{VSVERSION} == 2015      # If you remove the 'fail_build', you may see
   fail_build=1               # Internal Compiler errors.
%endif

510.parest_r:         # skip: does not build tested versions of Visual Studio
   fail_build=1       #    Visual Studio 2015 - Internal Compiler error
                      #    Visual Studio 2017 and 2019 - Compile error
   # If you try it (perhaps with a later compiler version), these flags may help.
   # but for now they are, by default, commented out.
   #   CXXPORTABILITY = /DDEAL_II_MEMBER_ARRAY_SPECIALIZATION_BUG /DDEAL_II_MEMBER_VAR_SPECIALIZATION_BUG

523.xalancbmk_r,623.xalancbmk_s:
   CXXPORTABILITY = -Zc:wchar_t-

525.x264_r,625.x264_s:
   CPORTABILITY   = -DSPEC_WINDOWS_MSVS2013 # This flag is needed for 2013 and for later versions too

526.blender_r:
   PORTABILITY_LIBS = shell32.lib advapi32.lib user32.lib
   NEEDATFILE       = 1

541.leela_r,641.leela_s:
   CXXPORTABILITY = -DBOOST_HAS_TR1_ARRAY

557.xz_r,657.xz_s:
   CPORTABILITY   = -DSPEC_NO_SSIZE_T -Drestrict=__restrict


#-------- Tuning Flags common to Base and Peak --------------------------------

#
# Speed (OpenMP and Autopar allowed)
#
%if %{bits} == 32
   intspeed,fpspeed:
       #
       # Many of the speed benchmarks (6nn.benchmark_s) do not fit in 32 bits
       # If you wish to run SPECint2017_speed or SPECfp2017_speed, please use
       #
       #     runcpu --define bits=64
       #
       fail_build = 1
%else
   intspeed,fpspeed:
       EXTRA_OPTIMIZE = /Qpar /DSPEC_OPENMP /openmp
%endif
#--------  Baseline Tuning Flags ----------------------------------------------
default=base:
    CXXOPTIMIZE        = /EHsc
%   if %{bits} == 32
        OPTIMIZE       = /Zi /O2
%   else
        OPTIMIZE       = /Zi /O2
%   endif

intrate,intspeed=base:           # 502.gcc_r/602.gcc_s need a large stack.  For
   EXTRA_LDFLAGS  = /F1500000    # base, set it for all benchmarks, as required.
                                 # http://www.spec.org/cpu2017/runrules.html#BaseFlags


#--------  Peak Tuning Flags ----------------------------------------------
default:           # If you develop some peak tuning, remove these two lines
   basepeak = yes  # and put your tuning in the default=peak: section below.
default=peak:

#------------------------------------------------------------------------------
# Tester and System Descriptions - EDIT all sections below this point
#------------------------------------------------------------------------------
#   For info about any field, see
#             https://www.spec.org/cpu2017/Docs/config.html#fieldname
#   Example:  https://www.spec.org/cpu2017/Docs/config.html#hw_memory
#------------------------------------------------------------------------------

#--------- EDIT to match your version -----------------------------------------
default:
   sw_compiler001   = C/C++: Version 19.21.27702.2 of Microsoft 
   sw_compiler002   = Visual Studio 2019

#--------- EDIT info about you ------------------------------------------------
# To understand the difference between hw_vendor/sponsor/tester, see:
#     https://www.spec.org/cpu2017/Docs/config.html#test_sponsor
intrate,intspeed,fprate,fpspeed: # Important: keep this line
   hw_vendor          = My Corporation
   tester             = My Corporation
   test_sponsor       = My Corporation
   license_num        = nnn (Your SPEC license number)
#  prepared_by        = # Ima Pseudonym                       # Whatever you like: is never output


#--------- EDIT system availability dates -------------------------------------
intrate,intspeed,fprate,fpspeed: # Important: keep this line
                        # Example                             # Brief info about field
   hw_avail           = # Nov-2099                            # Date of LAST hardware component to ship
   sw_avail           = # Nov-2099                            # Date of LAST software component to ship
   fw_bios            = # Version Mumble released May-2099    # Firmware information

#--------- EDIT system information --------------------------------------------
intrate,intspeed,fprate,fpspeed: # Important: keep this line
                        # Example                             # Brief info about field
#  hw_cpu_name        = # Intel Xeon E9-9999 v9               # chip name
   hw_cpu_nominal_mhz = # 9999                                # Nominal chip frequency, in MHz
   hw_cpu_max_mhz     = # 9999                                # Max chip frequency, in MHz
   hw_disk            = # 9 x 9 TB SATA III 9999 RPM          # Size, type, other perf-relevant info
#  hw_model           = # TurboBlaster 3000                   # system model name
#  hw_nchips          = # 99                                  # number chips enabled
#  hw_ncores          = # 9999                                # number cores enabled
   hw_ncpuorder       = # 1-9 chips                           # Ordering options  
#  hw_nthreadspercore = # 9                                   # number threads enabled per core
   hw_other           = # TurboNUMA Router 10 Gb              # Other perf-relevant hw, or "None"

   hw_memory001       = # 4 TB (256 x 16 GB 2Rx4 PC4-2133P-R, # N GB (M x N GB nRxn 
   hw_memory002       = # running at 1600 MHz)                # PCn-nnnnnR-n[, ECC and other info])
                     
   hw_pcache          = # 99 KB I + 99 KB D on chip per core  # Primary cache size, type, location  
   hw_scache          = # 99 KB I+D on chip per 9 cores       # Second cache or "None" 
   hw_tcache          = # 9 MB I+D on chip per chip           # Third  cache or "None" 
   hw_ocache          = # 9 GB I+D off chip per system board  # Other cache or "None" 

   sw_file            = # NTFS                                # File system  
#  sw_os001           = # Windows 99.9 Sailboat               # Operating system
#  sw_os002           = # Edition (64-bit) SP1                # 
   sw_other           = # TurboHeap Library V8.1              # Other perf-relevant sw, or "None"
   sw_state           = # Safe Mode, Virus scanner disabled   # Software state.  If no changes, "Default"

   power_management   = # briefly summarize power settings 

# Note: Some commented-out fields above are automatically set to preliminary 
# values by sysinfo
#       https://www.spec.org/cpu2017/Docs/config.html#sysinfo
# Uncomment lines for which you already know a better answer than sysinfo 

__HASH__
508.namd_r=base=mytest-VS2019-x64:
# Last updated 2022-11-09 19:28:24
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=918f1bcd31fe39ef6a8f4154074344f9ba7dafa543d259ee3689dcbbb921460e

511.povray_r=base=mytest-VS2019-x64:
# Last updated 2022-11-09 19:29:43
opthash=9b325e674d4ac7514a56dc792cce93d16582882724d0c72c1aaa0ba8a85f5475
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=6933f709f29121648892ce93601a3fafa10a665d2e864deb6ff5f3e886688a1c

519.lbm_r=base=mytest-VS2019-x64:
# Last updated 2022-11-09 19:29:50
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=be1c23fc03fd84806aef3bc58e9d4a893df3b1c8e33eda7e92371ff21d543a3c

526.blender_r=base=mytest-VS2019-x64:
# Last updated 2022-11-09 19:48:30
opthash=2db5c170816609c845d2a5585759395dc7a8c84d6f2a5f4e75e56cd827f585b4
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=737b2a6cc47f169477804a41e7fc1a8928fa370af01f71b81f559ed041112ae2

538.imagick_r=base=mytest-VS2019-x64:
# Last updated 2022-11-09 19:51:13
opthash=57824f05e03072fc2aa40404ae897311d29e91ac109c1194531a57cf62d52d20
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=69618a5e8c41fb7466214b913583e1fc33eebacf2d5ddae1ba8a42b61e984ba3

544.nab_r=base=mytest-VS2019-x64:
# Last updated 2022-11-09 19:51:30
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=45d0392de1faf5bf6d90796966444225e39c84ad9d561e4a7893b1d7be853397

997.specrand_fr=base=mytest-VS2019-x64:
# Last updated 2022-11-09 19:51:37
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=28b6a8077756d5d490b13cc5223354ad676e4c1ab8de533a0d2ec969f26a0260

600.perlbench_s=base=mytest-VS2019-x64:
# Last updated 2022-11-09 20:39:49
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=fe95fabfb7f4e1a52d6e34d12e7cf24afc93cd4eece27b7a406435ae2d2feaef

602.gcc_s=base=mytest-VS2019-x64:
# Last updated 2022-11-09 20:44:44
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=ef3ee20d8498c9594b52789ed0d2e5f0ef4eb1cbb8f764d8f591ec1f67c5cc77

605.mcf_s=base=mytest-VS2019-x64:
# Last updated 2022-11-09 20:44:53
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=9b4eb41884d955ca81b4df9e50d2dbd3344f837522fb0a054fcc79992183e92a

620.omnetpp_s=base=mytest-VS2019-x64:
# Last updated 2022-11-09 20:50:18
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=a28b24e85a48edf7061a205481a5b73719342e155f2538c44711c09126c67dd1

623.xalancbmk_s=base=mytest-VS2019-x64:
# Last updated 2022-11-09 21:02:42
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=50c38e0bd8851dcfd0c9d094e525293b11f6cae993553fabc07c4f55608792cb

625.x264_s=base=mytest-VS2019-x64:
# Last updated 2022-11-09 21:04:13
opthash=58c358d74b6d1d991b9b0b40e94a90746b579d3dd1b9c595619dd654b90b182d
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=2e6fa74de1a2f84ca476a45be9be98b704fc84851bad409264f94c609f625bff

641.leela_s=base=mytest-VS2019-x64:
# Last updated 2022-11-09 21:05:02
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=277427be23dbc2edae942c365215058965efff8dd7748c388967a1fa872fa2d5

657.xz_s=base=mytest-VS2019-x64:
# Last updated 2022-11-09 21:05:40
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=2b9b036d7e05a790bcb05de5cff270aa96c63c30d4aeb5a63fc13624bfd95f21

631.deepsjeng_s=base=mytest-VS2019-x64:
# Last updated 2022-11-10 14:45:18
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=8d7a62bf6a411d277a6d9dbdc171eca771d416d41118e493dd268221f7cb2e40

619.lbm_s=base=mytest-VS2019-x64:
# Last updated 2022-11-10 16:43:29
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=3913c28d91a374d5971daf509f31ed0764cb4dd87e3de7558b3097770b3999b7

638.imagick_s=base=mytest-VS2019-x64:
# Last updated 2022-11-10 16:46:03
opthash=ace63328b34289c85319006b0f282bb9be57c3aa56980c20792967094baf6787
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=2f6703190882c178eaaa2f26ce1a9a7a279e26b9f3b9e12e50447a0a1cc34d5f

644.nab_s=base=mytest-VS2019-x64:
# Last updated 2022-11-10 16:46:18
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=4d4d3b156dfe53dfc77a60ebdb7b35bc949ef8184ebbdf046665dbb4f904265e

500.perlbench_r=base=mytest-VS2019-x64:
# Last updated 2022-11-23 11:55:55
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=51bdaca4a8732e168a6601f6aded00799a9f2fd9e4830ed0eea468c0357caa74

502.gcc_r=base=mytest-VS2019-x64:
# Last updated 2022-11-23 12:00:44
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=6dada38f1508bbb2c37fccc9b5b0401b132a113f36f08e3ddc7bfff4242aa0ca

505.mcf_r=base=mytest-VS2019-x64:
# Last updated 2022-11-23 12:00:53
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=985a58c627200f3548c1d728b54992d32f1031366645dcc3ff31d579d7a90f04

520.omnetpp_r=base=mytest-VS2019-x64:
# Last updated 2022-11-23 12:06:38
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=a5e985e4ce5e9f22078110c51341ca3954a6b82200c4b408c0f924cb717e50e9

523.xalancbmk_r=base=mytest-VS2019-x64:
# Last updated 2022-11-23 12:19:28
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=36a86bf341074560fafe837b8397d6aa80f4abe7f1a1c036b47421a55ac80484

525.x264_r=base=mytest-VS2019-x64:
# Last updated 2022-11-23 12:21:03
opthash=940907e87f63d93ec705142d8487871319b08ff409fb20539b8284924bbcc45f
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=538d0d989e3dde297b6db48507f0c6551c5a6defe8d1d34bc4963740c0a62aef

541.leela_r=base=mytest-VS2019-x64:
# Last updated 2022-11-23 12:21:58
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=f94c8d725fb9d9a66fe9da1a3500d5fa8eb55c36892297efe81594e44fefbdb1

557.xz_r=base=mytest-VS2019-x64:
# Last updated 2022-11-23 12:22:37
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=bb7c777838925132875c4198967d34a0a1a60af6be98cdf7d1cfada2ee7f39dd

531.deepsjeng_r=base=mytest-VS2019-x64:
# Last updated 2022-11-28 09:52:12
opthash=304984de6d237f37d2c88a0907ccfad9dbf009e7cbed83b5d8dd7dd8f20e1c79
baggage=
compiler_version=\
*
compile_options=\
*Tm9uLW1ha2VmaWxlIG9wdGlvbnM6CnN0cmljdF9ydW5kaXJfdmVyaWZ5PSIxIgp2ZXJzaW9uPSIx\
LjAwMCIK
exehash=0154916ccb7bab4b5b529fcf3e6689c90b3e359786e8b08076931824da3902b5

