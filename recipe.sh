#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 3 ] || die "usage: out_dir"
out_dir=$1; shift
compiler=$1; shift
benchmark=$1; shift

echo "Running for $compiler benchmark $benchmark..."

script_dir=$(dirname $(readlink -f $0))

export SSH_PRIVATE_KEY_=$(cygpath -m "$SSH_PRIVATE_KEY")
export PUB_DIR=$(dirname "$SSH_PUBLIC_KEY")
mv $SSH_PUBLIC_KEY $PUB_DIR\\SSH_PRIVATE_KEY.pub

checkout()
{
    max_retries=5
    for i in $(seq 1 $max_retries)
    do
        git clone ssh://git@dev-private-git.linaro.org/restricted-benchmarks/CPU2017.git --config core.sshCommand="ssh -i $SSH_PRIVATE_KEY_ -o StrictHostKeyChecking=no -o UserKnownHostsFile=known"
        if [[ $? -eq 0 ]]
        then
            echo "Clone successful"
            break
        else
            echo "Clone failed, trying again in 30s"
            sleep 30
        fi
    done
}

setup()
{
    cd CPU2017
    ./install.bat
    cp $script_dir/msvc.cfg ./config/msvc.cfg
    cp $script_dir/clang-woa.cfg ./config/clang-woa.cfg
    python $script_dir/parse.py shrc.bat > shrc2.bat
    rm shrc.bat
    mv shrc2.bat shrc.bat
    python -m pip install requests
}

run()
{
    cmd.exe /c "shrc.bat && runcpu -n 4 -c $compiler ${benchmark}_no_fortran"
    cd result
    cp -R ../result $out_dir/
    python $script_dir/parse_results.py $SQUAD_ACCOUNT_KEY $out_dir $compiler
}

checkout
setup
run